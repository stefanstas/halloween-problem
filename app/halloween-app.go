package main

import (
    "fmt"
    "halloween-problem/app/model"
    "io/ioutil"
    "halloween-problem/app/service"
)

func main()  {
    houses, err := ReadResource("./data/houses.json")
    if err != nil {
        panic(err)
    }
    paths, err := ReadResource("./data/paths.json")
    if err != nil {
        panic(err)
    }

    steps := service.NewPathFinder(houses, paths).FindPerfectRoute("a", 300)
    PrintSteps(steps)
}

func PrintSteps(steps []model.Step)  {
    for i, step := range steps {
        if i == 0 {
            fmt.Printf("Starting from %s: %d sweets\n", step.GetTargetId(), step.GetSweetsCollected())
        } else {
            fmt.Printf("Move to  %s: %d sweets, %f meters\n", step.GetTargetId(), step.GetSweetsCollected(), step.GetDistanceTraveled())
        }
    }
    fmt.Printf("Total sweets: %d, %f meters\n", 0, 0.0) // TODO: calculate totals
}

func ReadResource(path string) ([]byte, error) {
    dat, err := ioutil.ReadFile(path)
    if err != nil {
        return nil, err
    }

    return dat, nil
}