package model

type Step struct {
    TargetId         string
    DistanceTraveled float64
    SweetsCollected  int
}

func NewStep(targetId string, distanceTraveled float64, sweetsCollected int) *Step {
    return &Step{ TargetId: targetId, DistanceTraveled: distanceTraveled, SweetsCollected: sweetsCollected }
}

func (s *Step) GetTargetId() string {
    return s.TargetId
}

func (s *Step) GetDistanceTraveled() float64 {
    return s.DistanceTraveled
}

func (s *Step) GetSweetsCollected() int {
    return s.SweetsCollected
}